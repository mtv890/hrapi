package com.cidenet.hrapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalErrorServerException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InternalErrorServerException(String message) {
        super(message);
    }
}

