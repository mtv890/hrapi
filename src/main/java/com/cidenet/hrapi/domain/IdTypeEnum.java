package com.cidenet.hrapi.domain;

public enum IdTypeEnum {
    CC("Cedula de Ciudadania"),
    CE("Cedula de Extranjeria"),
    PS("Pasaporte"),
    PE("Permiso Especial");

    public final String label;

    private IdTypeEnum(String label) {
        this.label = label;
    }
}
