package com.cidenet.hrapi.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

/**
 * Employee POJO
 */


@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Entity
@IdClass(EmployeeId.class)
public class EmployeeVO {
    //Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ
    //Es requerido y su longitud máxima será de 20 letras.
    @NotBlank
    @Pattern(regexp = "^[A-Z]{1,20}", message = "Invalid firstName")
    @Column(nullable = false)
    @ApiModelProperty(required = true, example = "LIONEL")
    private String firstName;

    //Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ, y el caracter espacio entre nombres.
    //Es opcional y su longitud máxima será de 50 letras.
    @Column(nullable = true)
    @Pattern(regexp = "^[A-Z\\s]{0,50}", message = "Invalid additionalName")
    @ApiModelProperty(required = false, example = "ANDRES")
    private String additionalName;

    //Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ.
    //Es requerido y su longitud máxima será de 20 letras.
    @NotBlank
    @Pattern(regexp = "^[A-Z]{1,20}", message = "Invalid firstSurname")
    @Column(nullable = false)
    @ApiModelProperty(required = true, example = "MESSI")
    private String firstSurname;

    //Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ.
    //Es requerido y su longitud máxima será de 20 letras.
    @NotBlank
    @Pattern(regexp = "^[A-Z]{1,20}", message = "Invalid secondSurname")
    @Column(nullable = false)
    @ApiModelProperty(required = true, example = "CUCCITTINI")
    private String secondSurname;

    //País para el cual el empleado prestará sus servicios, podrá ser Colombia o Estados Unidos.
    @NotNull
    @Column(nullable = false)
    @ApiModelProperty(required = true, example = "US", allowableValues = "CO,US")
    private EmploymentCountryEnum employmentCountry;

    //Es el número de identificación del empleado, debe ser alfanumérico permitiendo los siguientes conjuntos de caracteres (a-z / A-Z / 0-9 / -).
    //No podrán existir dos empleados con el mismo número y tipo de identificación. Su longitud máxima serán 20 caracteres.
    @Column(nullable = false)
    @ApiModelProperty(required = true, example = "CC", allowableValues = "CC,CE,PS,PE")
    @Id
    private IdTypeEnum typeId;

    @Id
    @ApiModelProperty(required = true, example = "MESSIAS")
    @Pattern(regexp = "^[A-Za-z0-9-]{1,20}", message = "Invalid id")
    private String id;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(nullable = false)
    @ApiModelProperty(required = true, example = "2022-09-25")
    @NotNull
    private LocalDate fromDate;


    //Será el área para la cual fue contratado el empleado, puede ser, Administración, Financiera, Compras, Infraestructura,
    //Operación, Talento Humano, Servicios Varios, etc. Debe ser una lista desplegable en la que el usuario seleccione el área correspondiente.
    @Column(nullable = false)
    @ApiModelProperty(required = true, example = "ADMIN", allowableValues = "ADMIN, FIN, HR, INFRA, OP, SALES, SVC")
    @NotNull
    private DepartmentEnum department;

}
