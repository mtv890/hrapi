package com.cidenet.hrapi.domain;

public enum DepartmentEnum {
    ADMIN("Administracion"),
    FIN("Financiera"),
    SALES("Compras"),
    INFRA("Infraestructura"),
    OP("Operaciones"),
    HR("Talento Humano"),
    SVC("Servicios Varios");

    public final String label;

    private DepartmentEnum(String label) {
        this.label = label;
    }
}
