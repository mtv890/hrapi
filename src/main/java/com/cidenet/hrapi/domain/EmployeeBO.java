package com.cidenet.hrapi.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
public class EmployeeBO extends EmployeeVO {

    /*
    <PRIMER_NOMBRE>.<PRIMER_APELLIDO>.<ID>@<DOMINIO>
    El DOMINIO será cidenet.com.co para Colombia y cidenet.com.us para Estados Unidos.
    No podrán existir 2 empleados con el mismo correo electrónico, en caso de que ya exista se debe agregar un valor numérico adicional secuencial (ID).
     */
    @Column(nullable = false)
    @Email
    @Max(300)
    private String email;

    // Siempre será Activo, no podrá ser modificado por el usuario.
    @Column(nullable = false)
    @ApiModelProperty(required = true, example = "ACTIVE", allowableValues = "ACTIVE")
    private StatusEnum status;

    //Fecha y hora de registro: Mostrará la fecha y hora del registro en formato DD/MM/YYYY HH:mm:ss, no puede ser editado.
    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime timeStamp;


    public EmployeeBO(EmployeeVO e, String email, StatusEnum status, LocalDateTime timeStamp) {
        super();
        this.setTypeId(e.getTypeId());
        this.setId(e.getId());

        this.setFirstName(e.getFirstName());
        this.setAdditionalName(e.getAdditionalName());
        this.setSecondSurname(e.getSecondSurname());
        this.setFirstSurname(e.getFirstSurname());

        this.setEmploymentCountry(e.getEmploymentCountry());
        this.setDepartment(e.getDepartment());
        this.setFromDate(e.getFromDate());

        this.setStatus(status);
        this.setEmail(email);
        this.setTimeStamp(timeStamp);
    }
}
