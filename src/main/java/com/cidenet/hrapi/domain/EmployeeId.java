package com.cidenet.hrapi.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Entity
@Data
//Es el número de identificación del empleado, debe ser alfanumérico permitiendo los siguientes conjuntos de caracteres (a-z / A-Z / 0-9 / -).
//No podrán existir dos empleados con el mismo número y tipo de identificación. Su longitud máxima serán 20 caracteres.
public class EmployeeId implements Serializable {

    @Column(nullable = false)
    @ApiModelProperty(required = true, example = "CE", allowableValues = "CC,CE,PS,PE")
    private IdTypeEnum typeId;

    @Id
    @Pattern(regexp = "^[A-Za-z0-9-]{1,20}")
    @ApiModelProperty(required = true, example = "MESSIAS")
    private String id;

}
