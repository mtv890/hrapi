package com.cidenet.hrapi.domain;

public enum EmploymentCountryEnum {
        US("Estados Unidos"),
        CO("Colombia");

        public final String label;

        private EmploymentCountryEnum(String label) {
                this.label = label;
        }
}
