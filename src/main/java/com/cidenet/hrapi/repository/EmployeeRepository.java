package com.cidenet.hrapi.repository;

import com.cidenet.hrapi.domain.EmployeeBO;
import com.cidenet.hrapi.domain.IdTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository<F> extends JpaRepository<EmployeeBO, String> {
    Optional<EmployeeBO> findEmployeeByIdAndTypeId(String id, IdTypeEnum typeId);

    List<EmployeeBO> findEmployeesByFirstNameAndFirstSurname(String firstName, String firstSurname);

    boolean existsByIdAndTypeId(String id, IdTypeEnum typeId);
}
