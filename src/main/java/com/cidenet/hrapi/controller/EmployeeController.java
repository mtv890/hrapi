package com.cidenet.hrapi.controller;

import com.cidenet.hrapi.domain.EmployeeBO;
import com.cidenet.hrapi.domain.EmployeeVO;
import com.cidenet.hrapi.domain.IdTypeEnum;
import com.cidenet.hrapi.service.IEmployeeService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/employees")
public class EmployeeController {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    private IEmployeeService employeeService;


    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{typeId}/{id}")
    @ApiOperation("Get an Employee by id")
    public EmployeeBO getEmployeeById(@PathVariable String id, @PathVariable IdTypeEnum typeId) throws Throwable {
        return employeeService.getEmployeeById(id, typeId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    @ApiOperation("Add an Employee")
    public EmployeeBO addEmployee(@Valid @RequestBody EmployeeVO employee) throws Throwable {
        return employeeService.addEmployee(employee);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @PutMapping()
    @ApiOperation("Edit an Employee")
    public EmployeeBO editEmployee(@Valid @RequestBody EmployeeVO employee) throws Throwable {
        return employeeService.editEmployee(employee);
    }

}
