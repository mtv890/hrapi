package com.cidenet.hrapi.service;

import com.cidenet.hrapi.domain.EmployeeBO;
import com.cidenet.hrapi.domain.EmployeeId;
import com.cidenet.hrapi.domain.EmployeeVO;
import com.cidenet.hrapi.domain.IdTypeEnum;

public interface IEmployeeService {
    EmployeeBO addEmployee(EmployeeVO e) throws Throwable;

    EmployeeBO editEmployee(EmployeeVO e) throws Throwable;

    EmployeeBO getEmployeeById(String id, IdTypeEnum typeId) throws Throwable;

}
