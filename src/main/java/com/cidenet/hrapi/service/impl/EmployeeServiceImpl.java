package com.cidenet.hrapi.service.impl;

import com.cidenet.hrapi.controller.EmployeeController;
import com.cidenet.hrapi.domain.*;
import com.cidenet.hrapi.exception.DataNotFoundException;
import com.cidenet.hrapi.exception.InternalErrorServerException;
import com.cidenet.hrapi.exception.InvalidDataException;
import com.cidenet.hrapi.repository.EmployeeRepository;
import com.cidenet.hrapi.service.IEmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements IEmployeeService {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);
    public static final String DOMAIN_CO = "@cidenet.com.co";
    public static final String DOMAIN_US = "@cidenet.com.us";

    @Autowired
    EmployeeRepository employeeRepository;

    @Transactional
    public EmployeeBO addEmployee(EmployeeVO e) throws Throwable {

        if (!isFromDateValid(e)) {
            LOG.info("Error when adding new Employee. " + e.getTypeId() + ":" + e.getId() + " already in DB");
            throw new InvalidDataException("Error when adding new Employee. " + e.getTypeId() + ":" + e.getId() + " fromDate mustn't be older than 30 days");
        }

        //If Employee is present in DB should use editEmployee instead
        if (isEmployeeInDB(e.getId(), e.getTypeId())) {
            LOG.info("Error when adding new Employee. " + e.getTypeId() + ":" + e.getId() + " already in DB");
            throw new InvalidDataException("Error when adding new Employee. " + e.getTypeId() + ":" + e.getId() + " already in DB");
        }

        try {
            LocalDateTime timeStamp = LocalDateTime.now();
            timeStamp.format(DateTimeFormatter.ofPattern("dd/MM/uuuu HH:mm:ss"));
            final StatusEnum status = StatusEnum.ACTIVE;

            EmployeeBO employeeBO = new EmployeeBO(e, generateEmail(e), status, timeStamp);
            employeeRepository.save(employeeBO);
            return employeeBO;
        } catch (Exception ex) {
            LOG.error("Error when adding Employee to DB" + ex.getMessage());
            throw new InternalErrorServerException("Error when adding Employee to DB");
        }
    }

    @Transactional
    public EmployeeBO editEmployee(EmployeeVO e) throws Throwable {

        if (!isFromDateValid(e)) {
            LOG.info("Error when editing Employee. " + e.getTypeId() + ":" + e.getId() + " already in DB");
            throw new InvalidDataException("Error when editing Employee. " + e.getTypeId() + ":" + e.getId() + " fromDate mustn't be older than 30 days");
        }

        //If Employee not present in DB should use addEmployee instead
        if (!isEmployeeInDB(e.getId(), e.getTypeId())) {
            LOG.info("Error when editing Employee. " + e.getTypeId() + ":" + e.getId() + " not found");
            throw new DataNotFoundException("Error when editing Employee. " + e.getTypeId() + ":" + e.getId() + " not found");
        }

        try {
            final LocalDateTime timeStamp = LocalDateTime.now();
            final StatusEnum status = StatusEnum.ACTIVE;

            EmployeeBO employeeBO = new EmployeeBO(e, generateEmail(e), status, timeStamp);
            employeeRepository.save(employeeBO);
            return employeeBO;
        } catch (Exception ex) {
            LOG.error("Error when editing Employee to DB");
            throw new InternalErrorServerException("Error when adding Employee to DB. " + ex.getMessage());
        }
    }

    @Transactional
    public EmployeeBO getEmployeeById(String id, IdTypeEnum typeId) throws Throwable {
        return (EmployeeBO) employeeRepository.findEmployeeByIdAndTypeId(id, typeId).orElseThrow((() -> {
            LOG.info("There is no Employee with id: " + typeId + ":" + id);
            return new DataNotFoundException("Employee " + typeId + ":" + id + " Not Found");
        }));
    }

    private boolean isFromDateValid(EmployeeVO e) {
        return e.getFromDate().isAfter(LocalDate.now().minusDays(30)) &&
                (e.getFromDate().isBefore(LocalDate.now()) || e.getFromDate().isEqual(LocalDate.now()));
    }

    private boolean isEmployeeInDB(String id, IdTypeEnum typeId) {
        return employeeRepository.existsByIdAndTypeId(id, typeId);
    }

    @SuppressWarnings("unchecked")
    private String generateEmail(final EmployeeVO e) {
        String domain;
        switch (e.getEmploymentCountry()) {
            case US:
                domain = DOMAIN_US;
                break;
            case CO:
                domain = DOMAIN_CO;
                break;
            default:
                throw new InternalErrorServerException("Invalid Employment Country");
        }

        //Get all clashing candidates
        List<EmployeeBO> employeeBOList =
                employeeRepository.findEmployeesByFirstNameAndFirstSurname(e.getFirstName(), e.getFirstSurname());

        //Delete itself from check list
        employeeBOList.removeIf(s -> (e.getId().equals(s.getId()) && e.getTypeId() == s.getTypeId()));

        Map<String, EmployeeBO> map = employeeBOList.stream()
                .collect(Collectors.toMap(EmployeeBO::getEmail, Function.identity()));

        String tentativeEmail = e.getFirstName() + "." + e.getFirstSurname() + domain;
        int id = 0;
        while (map.containsKey(tentativeEmail)) {
            id++;
            tentativeEmail =
                    e.getFirstName() + "." + e.getFirstSurname().replace("\\s", "") + "." + id + domain;
        }
        return tentativeEmail;
    }
}

