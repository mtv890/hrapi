package com.cidenet.hrapi.controller;

import com.cidenet.hrapi.domain.*;
import com.cidenet.hrapi.service.impl.EmployeeServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    private final String CONTROLLER_BASE_URL = "/api/v1/employees";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeServiceImpl employeeService;

    private EmployeeBO validResponse;
    private EmployeeId validId;

    private ObjectMapper om = new ObjectMapper();

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        validResponse = new EmployeeBO();


        validResponse.setFirstName("LIONEL");
        validResponse.setAdditionalName("ANDRES");
        validResponse.setFirstSurname("MESSI");
        validResponse.setSecondSurname("CUDICINI");

        validResponse.setTypeId(IdTypeEnum.CE);
        validResponse.setId("MESSIAS");

        validResponse.setFromDate(LocalDate.now());
        validResponse.setDepartment(DepartmentEnum.INFRA);
        validResponse.setEmploymentCountry(EmploymentCountryEnum.US);
        validResponse.setEmail("LIONEL.MESSI@cidenet.com.us");
        validResponse.setStatus(StatusEnum.ACTIVE);

        validResponse.setTimeStamp(LocalDateTime.now());

        validId.setId("MESSIAS");
        validId.setTypeId(IdTypeEnum.CE);
    }


    @Test
    public void getEmployeeById() throws Throwable {
        when(employeeService.getEmployeeById(validId.getId(),validId.getTypeId())).thenReturn(validResponse);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get(CONTROLLER_BASE_URL);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andReturn();

        verify(employeeService, times(1)).getEmployeeById(validId.getId(),validId.getTypeId());
    }
}